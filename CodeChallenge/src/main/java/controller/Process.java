package controller;

import exception.ReadCsvException;
import lombok.SneakyThrows;
import model.CoinAssetModel;
import model.CoinHistoryModel;
import model.WalletItem;
import service.CSVRead;
import service.CoinCapClient;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.*;

public class Process {

    private DecimalFormat decimalFormat = new DecimalFormat("0.00");
    private static HashMap<String, String> resultAsset = new HashMap();
    private static HashMap<String, Double> resultPerformance = new HashMap<>();

    private ArrayList<WalletItem> walletItems;

    private CSVRead csvRead = new CSVRead();
    private static CoinCapClient coinCapClient = new CoinCapClient();

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
    private ThreadPoolExecutor executor = null;

    private void initExecutor() {
        executor = new ThreadPoolExecutor(3, 3, Long.MAX_VALUE, TimeUnit.NANOSECONDS, new ArrayBlockingQueue<Runnable>(3)) {
            @Override
            protected void terminated() {
                super.terminated();
                showResults();
            }
        };
    }

    private void ProcessThread() throws InterruptedException {
        System.out.println("Now is " + simpleDateFormat.format(new Date()));
        initExecutor();
        WalletItem item = null;

        while(!walletItems.isEmpty()) {
            if(item == null) {
                item = walletItems.remove(0);
            }

            System.out.println("Submitted request " + item.getSymbol() + " at " + simpleDateFormat.format(new Date()));

            try {
                executor.execute(new Task(item));
                item = null;
            } catch (Exception e) {
                System.out.println("(program hangs, waiting for some of the previous requests to finish)");
                Thread.sleep(1000);
            }
        }

        executor.shutdownNow();
    }

    public Process() throws ReadCsvException, InterruptedException {
        walletItems = csvRead.read();
        ProcessThread();
    }

    private void showResults() {
        System.out.println(String.format("total=%s,best_asset=%s,best_performance=%s,worst_asset=%s,worst_performance=%s",
                getCorrectFormat(resultPerformance.get("total")),
                resultAsset.get("bestAsset"),
                getCorrectFormat(resultPerformance.get("bestPerformance")),
                resultAsset.get("worstAsset"),
                getCorrectFormat(resultPerformance.get("worstPerformance"))));
    }

    private String getCorrectFormat(Double value) {
        return decimalFormat.format(value).replace(",", ".");
    }

    private static class Task implements Runnable {
        private WalletItem walletItem;

        public Task(WalletItem walletItem) {
            this.walletItem = walletItem;
        }

        @SneakyThrows
        @Override
        public void run () {
            synchronized(resultPerformance)
            {
                CoinAssetModel coinAssetModel = coinCapClient.getAsset(walletItem.getSymbol()).get(0);
                CoinHistoryModel coinHistoryModel = coinCapClient.getAssetsHistory(coinAssetModel.getId()).get(0);

                Double total = walletItem.getQuantity() * coinHistoryModel.getPriceUsd();
                Double performance = coinHistoryModel.getPriceUsd() / walletItem.getPrice();

                if(resultAsset.isEmpty()) {
                    resultAsset.put("bestAsset", walletItem.getSymbol());
                    resultAsset.put("worstAsset", walletItem.getSymbol());
                    resultPerformance.put("total", total);
                    resultPerformance.put("bestPerformance", performance);
                    resultPerformance.put("worstPerformance", performance);
                } else {
                    resultPerformance.put("total", resultPerformance.get("total") + total);

                    if(resultPerformance.get("bestPerformance") < performance) {
                        resultPerformance.put("bestPerformance", performance);
                        resultAsset.put("bestAsset", walletItem.getSymbol());
                    }

                    if(resultPerformance.get("worstPerformance") > performance) {
                        resultPerformance.put("worstPerformance", performance);
                        resultAsset.put("worstAsset", walletItem.getSymbol());
                    }
                }
            }
        }
    }
}
