package service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import exception.CoinCapException;
import model.CoinAssetModel;
import model.CoinHistoryModel;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class CoinCapClient {

    private MultiThreadedHttpConnectionManager connectionManager;

    public CoinCapClient() {
        connectionManager = new MultiThreadedHttpConnectionManager();
    }

    public ArrayList<CoinAssetModel> getAsset(String symbol) throws CoinCapException {
        ArrayList<CoinAssetModel> coinAssetModels = new ArrayList<>();

        HttpClient client = new HttpClient(connectionManager);
        GetMethod httpGet = new GetMethod("https://api.coincap.io/v2/assets?limit=1&search=" + symbol);
        httpGet.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        try {
            client.executeMethod(httpGet);
            ObjectMapper mapper = new ObjectMapper();

            JsonNode actualObj = mapper.readTree(httpGet.getResponseBodyAsString());
            ArrayNode arrayNode = (ArrayNode) actualObj.get("data");

            for (Iterator<JsonNode> it = arrayNode.elements(); it.hasNext(); ) {
                JsonNode node = it.next();
                CoinAssetModel coinAssetModel = mapper.convertValue(node, CoinAssetModel.class);
                coinAssetModels.add(coinAssetModel);
            }

            return coinAssetModels;
        } catch (IOException e) {
            throw new CoinCapException();
        } finally {
            httpGet.releaseConnection();
        }
    }

    public ArrayList<CoinHistoryModel> getAssetsHistory(String type) throws CoinCapException {
        ArrayList<CoinHistoryModel> coinAssetModels = new ArrayList<>();

        HttpClient client = new HttpClient(connectionManager);
        GetMethod httpGet = new GetMethod("https://api.coincap.io/v2/assets/" + type + "/history?interval=d1&start=1617753600000&end=1617753601000");
        httpGet.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");

        try {
            client.executeMethod(httpGet);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj = mapper.readTree(httpGet.getResponseBodyAsString());
            ArrayNode arrayNode = (ArrayNode) actualObj.get("data");

            for (Iterator<JsonNode> it = arrayNode.elements(); it.hasNext(); ) {
                JsonNode node = it.next();
                CoinHistoryModel coinHistoryModel = mapper.convertValue(node, CoinHistoryModel.class);
                coinAssetModels.add(coinHistoryModel);
            }

            return coinAssetModels;
        } catch (IOException e) {
            throw new CoinCapException();
        } finally {
            httpGet.releaseConnection();
        }
    }
}
