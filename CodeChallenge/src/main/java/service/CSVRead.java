package service;

import exception.ReadCsvException;
import model.WalletItem;

import java.io.*;
import java.util.ArrayList;
import java.util.stream.Stream;

public class CSVRead {

    private static final String COMMA_DELIMITER = ",";
    private ArrayList<WalletItem> walletItems = new ArrayList<>();

    public ArrayList<WalletItem> read() throws ReadCsvException {
        try (
                InputStream inputStream = this.getClass().getResourceAsStream("/wallet.csv");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                Stream<String> lines = bufferedReader.lines().skip(1);
        ) {
            lines.forEach((line) -> convert(line));
        } catch (IOException e) {
            throw new ReadCsvException();
        }

        return walletItems;
    }

    private void convert(String line) {
        String parts[] = line.split(COMMA_DELIMITER);
        WalletItem item = new WalletItem();
        item.setSymbol(parts[0]);
        item.setQuantity(Double.valueOf(parts[1]));
        item.setPrice(Double.valueOf(parts[2]));
        walletItems.add(item);
    }
}
