package exception;

public class ReadCsvException extends Exception {

    public ReadCsvException() {
        super("Read csv file error");
    }

}
