package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CoinAssetModel {
    private String id;
    private String rank;
    private String symbol;
    private String name;
    private Double supply;
    private Double maxSupply;
    private Double marketCapUsd;
    private Double volumeUsd24Hr;
    private Double priceUsd;
    private Double changePercent24Hr;
    private Double vwap24Hr;
    private String explorer;
}
