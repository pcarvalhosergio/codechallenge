package service;

import exception.CoinCapException;
import model.CoinAssetModel;
import model.CoinHistoryModel;
import org.junit.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class CoinCapClientTest {
    @Test
    public void getAsset_whenSendBtc_thenReturnBitcoin() throws CoinCapException {
        CoinCapClient coinCapClient = new CoinCapClient();
        ArrayList<CoinAssetModel> coinAssetModels = coinCapClient.getAsset("BTC");
        assertThat(coinAssetModels.get(0).getId()).isEqualTo("bitcoin");
    }

    @Test
    public void getAsset_whenSendEth_thenReturnEthereum() throws CoinCapException {
        CoinCapClient coinCapClient = new CoinCapClient();
        ArrayList<CoinAssetModel> coinAssetModels = coinCapClient.getAsset("ETH");
        assertThat(coinAssetModels.get(0).getId()).isEqualTo("ethereum");
    }

    @Test
    public void getAssetsHistory_whenSendBitcoin_thenReturnEthereum() throws CoinCapException {
        CoinCapClient coinCapClient = new CoinCapClient();
        ArrayList<CoinHistoryModel> coinHistoryModels = coinCapClient.getAssetsHistory("bitcoin");
        assertThat(coinHistoryModels.get(0).getPriceUsd()).isEqualTo(Double.valueOf(56999.9728252053067291));
    }
}
