package service;

import exception.ReadCsvException;
import model.WalletItem;
import org.junit.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class CSVReadTest {
    @Test
    public void read_whenRead_thenReturnWalletList() throws ReadCsvException {
        CSVRead cSVRead = new CSVRead();
        ArrayList<WalletItem> walletItems = cSVRead.read();
        assertThat(walletItems.get(0).getSymbol()).isEqualTo("BTC");
    }
}
